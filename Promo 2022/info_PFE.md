# Projet de Fin d'Etudes (PFE)

## Rapport

**Pré-rapport** : 6 semaines après le début du stage (< 20 pages)

**Rapport** : lundi qui précède la semaine de soutenance (< 40 pages)

Mise en forme :

- Taille caratère 12 points
- Marges 2,5 cm
- Interligne simple
- Numérotation de page

## Soutenance orale

Possibilités :

- Mi-juin
- Fin août (pour les alternants finissant leur contrat avant mi-septembre)
- Mi-septembre

Conditions :

- Maximum 2 semaines avant la fin du PFE
- Avant la fin du contrat (pour les alternants)

Jury :

- Tuteur de stage
- Tuteur pédagogique
- Enseignant président du jury (~ un responsable de filière)

Présentation :

- 30 minutes de présentation
- 15 minutes de question

## Confidentialité

Si le PFE est confidentiel il faut prévenir le **tuteur pédagogique** et les **responsables de filières**. Il faudra penser à leur redire que le PFE est confidentiel au moment de la remise du rapport car il leur arrive d'oublier.

### Pré-rapport

Cela dépend de l'entreprise. Le pré-rapport peut être envoyé par mail au tuteur qui est tenu par la confidentialité. Le tuteur devra jeter toutes les versions imprimées et supprimer les mails concernés. Il est possible d'ajouter une mention "Confidentiel" qui s'ajoute au moment de l'impression pour que le tuteur s'en souvienne.

### Rapport

Cela dépend de l'entreprise. Le rapport peut être envoyé au format papier ou bien déposé sur e-stage (plus simple). Il est possible de demander les clés publiques de l'Ensimag pour chiffrer le rapport avant de le déposer.

Dans tous les cas, ils s'engagent à ne pas le diffuser et à le détruire une fois la soutenance terminée.

### Soutenance

Il faudra cocher la case "confidentiel" lors du choix de la soutenance. Elle ne sera alors pas publique mais à huis clos.

## Liens

* [Consignes exactes pour le(s) rapport(s)](https://intranet.ensimag.grenoble-inp.fr/medias/fichier/consignes-pour-la-redaction-des-rapports-de-stage-2018-2019_1618476221328-pdf?ID_FICHE=489069&INLINE=FALSE)
* [Modèle de page de garde](https://intranet.ensimag.grenoble-inp.fr/medias/fichier/modele-couv-pfe-2017_1497944381433-rtf?ID_FICHE=489069&INLINE=FALSE)
* [Consignes pour la partie environnement](https://intranet.ensimag.grenoble-inp.fr/medias/fichier/202105-pfe-reglesimpactenvironnementalsocietal_1623226095127-pdf?ID_FICHE=489069&INLINE=FALSE) = 5 points / 20 du **PFE**
* [Fiche d'évaluation pour le tuteur de stage](https://intranet.ensimag.grenoble-inp.fr/medias/fichier/ficheevaluation-2018-2019-stage-fr_1547539371504-pdf?ID_FICHE=489069&INLINE=FALSE)
