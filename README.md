# Ensimag - Cheat code

Projet pour partager tout ce qui pourrait être utile au bon déroulement d'une année étudiante à l'[Ensimag](https://ensimag.grenoble-inp.fr/). Vous y trouverez fiches de révision, anciens partiels... N'hésitez surtout pas à enrichir ce Git.
